# frozen_string_literal: true

class CreateBills < ActiveRecord::Migration[5.2]
  def change
    create_table :bills do |t|
      t.string :title
      t.string :labour_charge
      t.string :material_charge
      t.string :total_payable
      t.string :paid_amount
      t.string :balance_amount
      t.datetime :issued_at
      t.references :project, foreign_key: true
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
