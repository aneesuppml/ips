# frozen_string_literal: true

class CreateWorkers < ActiveRecord::Migration[5.2]
  def change
    create_table :workers do |t|
      t.string :name
      t.string :phone
      t.text :address
      t.string :daily_wage
      t.string :overtime_amount

      t.timestamps
    end
  end
end
