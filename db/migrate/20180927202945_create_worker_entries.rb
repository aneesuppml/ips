# frozen_string_literal: true

class CreateWorkerEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :worker_entries do |t|
      t.references :worker, foreign_key: true
      t.references :bill, foreign_key: true
      t.string :worked_hours

      t.timestamps
    end
  end
end
