// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require jquery_ujs
//= require rails-ujs
//= require turbolinks
//= require_tree .


(function ($) {
  "use strict";
  $.fn.responsiveTable = function () {
    var toggleColumns = function ($table) {
      var selectedControls = [];
      $table.find(".Accordion, .Tab").each(function () {
        selectedControls.push($(this).attr("aria-selected"));
      });
      var cellCount = 0, colCount = 0;
      var setNum = $table.find(".Rtable-cell").length / Math.max($table.find(".Tab").length, $table.find(".Accordion").length);
      $table.find(".Rtable-cell").each(function () {
        $(this).addClass("hiddenSmall");
        if (selectedControls[colCount] === "true") $(this).removeClass("hiddenSmall");
        cellCount++;
        if (cellCount % setNum === 0) colCount++;
      });
    };

    $(this).each(function () {
      toggleColumns($(this));
    });

    $(this).find(".Tab").click(function () {
      $(this).attr("aria-selected", "true").siblings().attr("aria-selected", "false");
      toggleColumns($(this).parents(".Rtable"));
    });

    $(this).find(".Accordion").click(function () {
      $(this).attr("aria-selected", $(this).attr("aria-selected") !== "true");
      toggleColumns($(this).parents(".Rtable"));
    });

  };
}(jQuery));


document.addEventListener("turbolinks:load", function () {
  $(".js-RtableTabs, .js-RtableAccordions").responsiveTable();


  $("#addNewWorker").click(function () {
    // console.log('clicked ............... ppp', $("#testChild").html())
    $("#worker-fields").append($("#worker-field").html());
  });
});

$(document).on("keyup", "#material-charge, #labour-charge", "#paid-amount", function () {
  var materiaCharge = $("#material-charge").val() || 0;
  var labourcharge = $("#labour-charge").val() || 0;
  var totalPayable = parseInt(materiaCharge) + parseInt(labourcharge)
  $("#total-payable").val(totalPayable);
});

$(document).on("keyup", "#paid-amount", function () {
  var totalPayable = $("#total-payable").val();
  var paidAmount = $("#paid-amount").val();
  var balanceAmount = totalPayable ? (totalPayable - paidAmount) : 0;
  $("#balance-amount").val(balanceAmount);
});