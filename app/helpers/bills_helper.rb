# frozen_string_literal: true

module BillsHelper
  def bill_sum_by(bills, key)
    bills.map(&key.to_sym).map(&:to_f).sum
  end
end
