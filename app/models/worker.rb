# frozen_string_literal: true

class Worker < ApplicationRecord
  has_many :worker_entries, inverse_of: :worker, dependent: :destroy
  has_many :bills, through: :worker_entries
  # has_many :bills, -> { distinct }, through: :worker_entries

  validates :name, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
  validates :address, :phone, presence: true
  validates :daily_wage, :overtime_amount, numericality: true
  validates :phone, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
  VALID_MOBILE_REGEX = /\A(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}\z/
  validates :phone, presence: true, length: { maximum: 255 },
                    format: { with: VALID_MOBILE_REGEX },
                    uniqueness: { case_sensitive: false }
end
