# frozen_string_literal: true

class WorkerEntry < ApplicationRecord
  belongs_to :worker, inverse_of: :worker_entries
  belongs_to :bill, inverse_of: :worker_entries

  validates :worked_hours, presence: true, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 24 }

  validates_uniqueness_of :worker_id, scope: :bill_id
end
