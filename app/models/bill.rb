# frozen_string_literal: true

class Bill < ApplicationRecord
  belongs_to :project
  belongs_to :client

  has_many :worker_entries, inverse_of: :bill, dependent: :destroy
  # has_many :workers, through: :worker_entries
  has_many :workers, -> { distinct }, through: :worker_entries

  accepts_nested_attributes_for :worker_entries

  validates :title, presence: true, length: { maximum: 50 }
  validates :labour_charge, presence: true
  validates :material_charge, presence: true
  validates :total_payable, presence: true
  validates :paid_amount, presence: true
  validates :balance_amount, presence: true
  validates :issued_at, presence: true

  validate :total_payable_is_greater
  validate :total_payable_is_correct

  scope :paid_bills, -> { select { |b| b.total_payable.to_f == b.paid_amount.to_f } }
  scope :pending_bills, -> { select { |b| b.total_payable.to_f != b.paid_amount.to_f } }

  def total_payable_is_greater
    errors.add(:paid_amount, 'No need to pay more than amount payable') if total_payable.to_f < paid_amount.to_f
  end

  def total_payable_is_correct
    sum_value = labour_charge.to_f + material_charge.to_f
    errors.add(:total_payable, 'Should be sum of material and labour charge') unless sum_value == total_payable.to_f
  end

  def number
    'PCB' + project_id.to_s + client_id.to_s + id.to_s
  end
end
