# frozen_string_literal: true

class Client < ApplicationRecord
  has_many :bills
  validates :name, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
  validates :address, presence: true
  validates :phone, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }

  def paid_bills
    bills.paid_bills
  end

  def pending_bills
    bills.pending_bills
  end
end
