# frozen_string_literal: true

class ClientsController < ApplicationController
  before_action :find_client, only: %i[show edit update destroy bills]

  def index
    @clients = Client.all
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new(client_params)
    if @client.save
      redirect_to clients_path, notice: 'The client was created!'
    else
      render 'new'
    end
  end

  def show; end

  def edit; end

  def update
    @client.update_attributes(client_params)
    redirect_to clients_path, notice: 'Updated successfully'
  end

  def destroy
    @client.destroy
    redirect_to clients_path, notice: 'Deleted successfully'
  end

  def bills
    @bills = @client.bills
    @pending_bills = @client.pending_bills
    @paid_bills = @client.paid_bills
  end

  private

  def client_params
    params.require(:client).permit(:name, :address, :phone)
  end

  def find_client
    client_id = params[:id] || params[:client_id]
    @client = Client.find_by_id(client_id)
  end
end
