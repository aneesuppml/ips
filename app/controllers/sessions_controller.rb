# frozen_string_literal: true

class SessionsController < ApplicationController
  def new
    render 'dashboard' if logged_in?
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user&.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in user
      render 'dashboard'
    else
      p user
      flash[:danger] = 'Invalid email/password combination' # Not quite right!
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

  def dashboard; end
end
