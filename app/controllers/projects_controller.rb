# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :find_project, only: %i[show edit update destroy bills]

  def index
    @projects = Project.all
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    if @project.save
      redirect_to projects_path, notice: 'The project was created!'
    else
      render 'new'
    end
  end

  def show; end

  def edit; end

  def update
    @project.update_attributes(project_params)
    redirect_to projects_path, notice: 'Updated successfully'
  end

  def destroy
    @project.destroy
    redirect_to projects_path, notice: 'Deleted successfully'
  end

  def bills
    @bills = @project.bills
    @pending_bills = @project.pending_bills
    @paid_bills = @project.paid_bills
  end

  private

  def project_params
    params.require(:project).permit(:name)
  end

  def find_project
    project_id = params[:id] || params[:project_id]
    @project = Project.find_by_id(project_id)
  end
end
