# frozen_string_literal: true

class WorkersController < ApplicationController
  before_action :find_worker, only: %i[show edit update destroy]

  def index
    @workers = Worker.all
  end

  def new
    @worker = Worker.new
  end

  def create
    @worker = Worker.new(worker_params)
    if @worker.save
      redirect_to workers_path, notice: 'The worker was created!'
    else
      render 'new'
    end
  end

  def show; end

  def edit; end

  def update
    @worker.update_attributes(worker_params)
    redirect_to workers_path, notice: 'Updated successfully'
  end

  def destroy
    @worker.destroy
    redirect_to workers_path, notice: 'Delete success'
  end

  private

  def worker_params
    params.require(:worker).permit(:name, :phone, :address, :daily_wage, :overtime_amount)
  end

  def find_worker
    @worker = Worker.find_by_id(params[:id])
  end
end
