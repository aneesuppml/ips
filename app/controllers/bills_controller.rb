# frozen_string_literal: true

class BillsController < ApplicationController
  before_action :find_bill, only: %i[show edit update destroy]

  def index
    @bills = Bill.all
  end

  def new
    @bill = Bill.new
  end

  def create
    @bill = Bill.new(bill_params)

    workers = params[:workers]
    worked_hours = params[:worked_hours]

    workers.each_with_index do |worker_id, index|
      @bill.worker_entries.build(worker_id: worker_id, worked_hours: worked_hours[index])
    end

    if @bill.save
      redirect_to bills_path, notice: 'The bill was created!'
    else
      render 'new'
    end
  end

  def show; end

  def edit; end

  def update
    @bill.update_attributes(bill_params)
    redirect_to bills_path, notice: 'Updated successfully'
  end

  def destroy
    @bill.destroy
    redirect_to bills_path, notice: 'Deleted successfully'
  end

  private

  def bill_params
    params.require(:bill).permit(:project_id, :client_id, :title, :labour_charge, :material_charge, :total_payable,
                                 :paid_amount, :balance_amount, :issued_at, worker_entries_attributes: %i[id worker_id worked_hours _destroy])
  end

  def find_bill
    @bill = Bill.find_by_id(params[:id])
  end
end
