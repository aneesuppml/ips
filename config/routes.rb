# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # root 'sessions#dashboard'
  # get    '/login',   to: 'sessions#new'
  # post   '/login',   to: 'sessions#create'
  # delete '/logout',  to: 'sessions#destroy'
  # get '/dashboard', to: 'sessions#dashboard'

  devise_scope :user do
    root 'sessions#dashboard', as: :authenticated_root
    resources :workers
    # resources :projects
    resources :clients do
      get 'bills', to: 'clients#bills'
    end
    resources :bills
    resources :projects do
      get 'bills', to: 'projects#bills'
    end
  end

  root 'devise/sessions#new'
end
